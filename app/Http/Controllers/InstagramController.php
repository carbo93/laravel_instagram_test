<?php

namespace App\Http\Controllers;

use Instagram;
use Illuminate\Http\Request;
class InstagramController extends Controller
{

    public function index(Request $request)
    {
        // Generate and redirect to login URL.
        
        $url = Instagram::getLoginUrl();

        return redirect($url);
      

    }


    public function callback(Request $request)
    {
        // Generate and redirect to login URL.
        //dd($request);
        
        $code = $request->get('code');
        //echo $code;
        $data = Instagram::getOAuthToken($code);
        

        
        // Now, you have access to authentication token and user profile
        echo 'Your username is: ' . $data->user->username;
        echo 'Your access token is: ' . $data->access_token;    
        
        $accessToken = $data->access_token;
        // Set user access token
        Instagram::setAccessToken($accessToken);

        // Get all user likes
        //$likes = Instagram::getUserLikes();
        $result = Instagram::getUser();
        //echo 'Your user likes is: ';

        //var_dump($result);

        $result = Instagram::searchMedia('48.858844','2.294351',500);
        
        dd($result);


    }

}
